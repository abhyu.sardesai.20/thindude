using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableSound : MonoBehaviour
{
    private AudioSource _audioSource;
    private bool _hasPlayed = false;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound()
    {
        if (!_hasPlayed)
        {
            _hasPlayed = true;
            _audioSource.Play();
            StartCoroutine(ResetSound());
        }
    }

    private IEnumerator ResetSound()
    {
        yield return new WaitForSeconds(5f);
        _hasPlayed = false;
    }
}
