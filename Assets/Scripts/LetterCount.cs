﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable/States/LetterCount")]
public class LetterCount : TState<List<string>>
{

}

