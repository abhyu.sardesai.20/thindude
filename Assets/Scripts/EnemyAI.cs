using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class EnemyAI : MonoBehaviour
{
    [SerializeField]
    private GameState _gameState;
    [SerializeField]
    private LetterCount _letterSO;
    [SerializeField]
    private PlayerState _pState;
    private States _state;

    public bool _isSpawned = false;
    public bool _canSpawn = true;
    [SerializeField]
    private float _despawnDistance = 10f;

    [SerializeField]
    private Transform _player;
    private Vector2 _playerPosition;
    private float _distanceToPlayer;
    //[SerializeField] 
    private float _baseSpeed = 1.2f;
    [SerializeField]
    private float _moveSpeed;
    private Collider2D[] _colliders;
    private SpriteRenderer _renderer;
    private bool _aiEnabled;
    private AudioSource _audioSource;
    private void OnEnable()
    {
        _gameState.Observers += ChangeState;
        _letterSO.Observers += SpeedChange;
    }

    private void OnDisable()
    {
        _gameState.Observers -= ChangeState;
        _letterSO.Observers -= SpeedChange;
    }
    
    private void ChangeState(States s)
    {
        _state = s;
    }

    private void SpeedChange(List<string> s)
    {
        if (s.Count == 3)
        {
            _baseSpeed = 1.5f;               
            Debug.Log(_moveSpeed);
        }else if (s.Count == 5)
        {
            _baseSpeed = 1.7f;
            Debug.Log(_moveSpeed);
        }else if (s.Count == 7)
        {
            _baseSpeed = 2f;
            Debug.Log(_moveSpeed);
        }
        _moveSpeed = _baseSpeed;
    }

    private void Awake()
    {
        _colliders = GetComponents<Collider2D>();
        _renderer = GetComponentInChildren<SpriteRenderer>();
        DisableEnemy();
        _moveSpeed = _baseSpeed;
        Debug.Log(_moveSpeed);
        _audioSource = GetComponent<AudioSource>();
    }

    private void DisableEnemy()
    {
        _aiEnabled = false;
        foreach (Collider2D collider in _colliders)
        {
            collider.enabled = false;
        }
        _renderer.enabled = false;
    }


    private void EnableEnemy()
    {
        _aiEnabled = true;

        foreach (Collider2D collider in _colliders)
        {
            collider.enabled = true;
        }
        _renderer.enabled = true;
    }

    private void Update()
    {
        if (_state == States.PAUSE) return;
        if (!_aiEnabled) return;
        //_moveSpeed = _baseSpeed;
        _playerPosition = _player.position;
        _distanceToPlayer = Vector2.Distance(_playerPosition, transform.position);
        Vector3 direction = (_playerPosition - (Vector2)transform.position).normalized;
        transform.position += direction * _moveSpeed * Time.deltaTime;
        if (_distanceToPlayer >= _despawnDistance)
        {
            DisableEnemy();
            _isSpawned = false;
            _canSpawn = true;
        }

        if (_pState.Value == PState.DANGER && _distanceToPlayer > 8f)
        {
            _pState.Value = PState.SAFE;
        } else if (_pState.Value == PState.SAFE && _distanceToPlayer < 8f)
        {
            _pState.Value = PState.DANGER;
        }
        
    }

    public void Spawn(Vector2 spawnPoint)
    {
        EnableEnemy();
        transform.position = spawnPoint;
        _isSpawned = true;
        _canSpawn = false;
        /*if (!_isSpawned && _canSpawn)
        {
            gameObject.SetActive(true);
            transform.position = spawnPoint;
            _isSpawned = true;
            _canSpawn = false;
        }*/
    }

    public void Stop()
    {
        StartCoroutine(ResetMovement());
    }

    public void DetectedSound()
    {
        if (!_audioSource.isPlaying)
        {
            _audioSource.Play();
        }
    }

    private IEnumerator ResetMovement()
    {
        yield return new WaitForSeconds(.4f);
        _moveSpeed = 0f;
        yield return new WaitForSeconds(5f);
        _moveSpeed = _baseSpeed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Fire"))
        {
            Debug.Log($"safe");
            DisableEnemy();
            _isSpawned = false;
            _canSpawn = true;
        }

        if (other.gameObject.GetComponent<TopDownController>() != null)
        {
            Debug.Log(_pState.Value);
            _pState.Value = PState.DEAD;                      
        }
    }
}
