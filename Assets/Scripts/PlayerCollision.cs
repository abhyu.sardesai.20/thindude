using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public ColliderGridCreator ColliderGridCreator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<TopDownController>() != null)
            ColliderGridCreator.EnteredNewCollider(this);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<TopDownController>() != null)
            ColliderGridCreator.InSameCollider(this);
    }
}
