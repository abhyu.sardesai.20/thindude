using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField]
    private SceneState _scene;
    [SerializeField]
    private GameState _gameState;
    [SerializeField]
    private KeyCount _keyCount;
    [SerializeField]
    private LetterCount _letterCount;
    [SerializeField]
    private BatteryCount _batCount;

    public void ChangeScene(int s)
    {
        _keyCount.Value.Clear();
        _letterCount.Value.Clear();
        _batCount.Value = 0;
        _gameState.Value = States.PLAY;
        _scene.Value = s;
        SceneManager.LoadScene(s);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
