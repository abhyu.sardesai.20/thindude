﻿using UnityEngine;

public class BatteryInteractable : Interactables
{
    [SerializeField]
    private BatteryCount _batSO;

    public override void OnInteracted()
    {
        _batSO.Value += 1;
        Collected();
    }
}