﻿using UnityEngine;

public class KeyInteractable : Interactables
{
    [SerializeField]
    private KeyCount _keySO;
    [SerializeField]
    private string _keyName;

    private void Awake()
    {
        if (_keySO.Value.Contains(_keyName))
        {
            Collected();
        }
    }

    public override void OnInteracted()
    {
        var keyList = _keySO.Value;
        if (keyList != null)
        {
            keyList.Add(_keyName);
            _keySO.Value = keyList;
        }
        Collected();
    }
}
