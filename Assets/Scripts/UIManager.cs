using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _deathMenu;
    [SerializeField]
    private GameObject _pauseMenu;
    [SerializeField]
    private GameObject _pagesMenu;
    [SerializeField]
    private GameState _gameState;
    [SerializeField]
    private PlayerState _pState;
    [SerializeField]
    private LetterCount _letterCount;

    [SerializeField]
    private List<Button> _buttons;
    [SerializeField]
    private List<Pages> _pages;
   

    private int _letterUnlocked;

    private void Awake()
    {
        _pauseMenu.SetActive(false);
        _deathMenu.SetActive(false);
    }

    private void OnEnable()
    {
        _gameState.Observers += CheckMenuState;
        _letterCount.Observers += DisplayLetter;
        _pState.Observers += DeathScreen;

    }

    private void DeathScreen(PState obj)
    {
        if (obj == PState.DEAD)
        {
            _deathMenu.SetActive(true);
        }
    }

    private void DisplayLetter(List<string> name)
    {
        _gameState.Value = States.PAUSE;
        _pauseMenu.SetActive(false);
        _pagesMenu.SetActive(true);
        foreach (var item in _pages)
        {
            if (item.LetterName == name[name.Count - 1])
            {
                item.gameObject.SetActive(true);
            }
        }
        _pauseMenu.SetActive(false);
    }

    private void OnDisable()
    {
        _gameState.Observers -= CheckMenuState;
        _letterCount.Observers -= DisplayLetter;
        _pState.Observers -= DeathScreen;
    }

    private void CheckMenuState(States s)
    {
        if (s == States.PAUSE)
        {
            _pauseMenu.SetActive(true);
            //CheckButtons();
        }
        else
        {
            _pauseMenu.SetActive(false);
            foreach (var item in _pages)
            {
                item.gameObject.SetActive(false);
            }
        }
    }

    private void CheckButtons()
    {
        foreach (var button in _buttons)
        {
            foreach (var item in _letterCount.Value)
            {
                if (button.GetComponent<ButtonName>().ButtonLetterName == item)
                {
                    button.gameObject.SetActive(true);
                }

            }
        }
    }
}
