using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popups : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _popups;
    private int _index = 0;

    private void Awake()
    {
        foreach (var item in _popups)
        {
            item.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            StartCoroutine(TextBoxes());
        }
    }

    private IEnumerator TextBoxes()
    {
        _popups[_index].SetActive(true);
        yield return new WaitForSeconds(5f);
        _popups[_index].SetActive(false);
        _index++;
        _popups[_index].SetActive(true);
        yield return new WaitForSeconds(5f);
        _popups[_index].SetActive(false);
        _index++;
        _popups[_index].SetActive(true);
        yield return new WaitForSeconds(5f);
        _popups[_index].SetActive(false);
        _index++;
        
    }
}
