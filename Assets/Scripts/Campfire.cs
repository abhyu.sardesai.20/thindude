using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Campfire : MonoBehaviour
{
    [SerializeField] 
    private GameState _gameState;
    [SerializeField] 
    private LetterCount _letterSO;
    private States _state;
    [SerializeField]
    private GameObject _litFire;
    [SerializeField]
    private GameObject _unlitFire;
    private FieldOfView _fov;

    private void Awake()
    {
        _fov = GetComponent<FieldOfView>();
        _litFire.SetActive(true);
        _unlitFire.SetActive(false);
    }

    private void OnEnable()
    {
        _gameState.Observers += ChangeState;
        _letterSO.Observers += DisableSafety;
    }

    private void OnDisable()
    {
        _gameState.Observers -= ChangeState;
        _letterSO.Observers -= DisableSafety;
    }
    
    private void ChangeState(States s)
    {
        _state = s;
    }

    private void DisableSafety(List<string> s)
    {
        if (s.Count >= 3)
        {
            _fov.SetViewDistance(0);
            GetComponent<CircleCollider2D>().enabled = false;
            _litFire.SetActive(false);
            _unlitFire.SetActive(true);
        }
    }

    private void Update()
    {
        if (_state == States.PAUSE) return;
    }
}
