using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public static List<Spawner> Instances = new List<Spawner>();

    public static Spawner GetRandom() => Instances[Random.Range(0, Instances.Count)];
    
    [SerializeField] 
    private Transform _player;
    private Vector2 _playerPosition;
    private float _distanceToPlayer;
    [SerializeField]
    private EnemyAI _enemy;
    [SerializeField]
    private TimerShared _sharedTimer;

    [SerializeField] 
    private float _minDistanceToSpawn = 6f;
    [SerializeField] 
    private float _maxDistanceToSpawn = 8f;   

    private void Awake()
    {
        Spawner.Instances.Add(this);
    }

    private void Update()
    {
        _playerPosition = _player.position;
        _distanceToPlayer = Vector2.Distance(_playerPosition, transform.position);

        if (_sharedTimer._timer >= _sharedTimer._timeToSpawn)
        {
            //foreach (var spawner in Instances)
            {
                if (_distanceToPlayer >= _minDistanceToSpawn && _distanceToPlayer <= _maxDistanceToSpawn)
                {
                    _enemy.Spawn(transform.position);
                }
            }
        }
        
    }
}
