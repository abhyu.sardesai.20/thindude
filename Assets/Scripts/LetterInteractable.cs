﻿using UnityEngine;

public class LetterInteractable : Interactables
{
    [SerializeField]
    private LetterCount _letterSO;
    [SerializeField]
    private string _letterName;

    private void Awake()
    {
        if (_letterSO.Value.Contains(_letterName))
        {
            Collected();
        }
    }

    public override void OnInteracted()
    {
        //_letterSO.Add(_letterName);
        var letterList = _letterSO.Value;
        if (letterList != null)
        {
            letterList.Add(_letterName);
            _letterSO.Value = letterList;
        }
        Collected();
    }
}
