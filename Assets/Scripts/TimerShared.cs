using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerShared : MonoBehaviour
{
    [SerializeField] 
    private GameState _gameState;
    [SerializeField] 
    private LetterCount _letterSO;
    private States _state;
    
    [SerializeField]
    private EnemyAI _enemy;
    [HideInInspector]
    public float _timeToSpawn = 10f;
    [HideInInspector]
    public float _timer;

    private void OnEnable()
    {
        _gameState.Observers += ChangeState;
        _letterSO.Observers += SpawnInterval;
    }

    private void OnDisable()
    {
        _gameState.Observers -= ChangeState;
        _letterSO.Observers -= SpawnInterval;
    }
    
    private void ChangeState(States s)
    {
        _state = s;
    }

    private void SpawnInterval(List<string> s)
    {
        if (s.Count == 3)
        {
            _timeToSpawn = 8f;
        }else if (s.Count == 5)
        {
            _timeToSpawn = 6f;
        }else if (s.Count == 7)
        {
            _timeToSpawn = 4f;
        }
    }
    private void Update()
    {
        if (_state == States.PAUSE) return;
        //Debug.Log(_timer);
        if (_enemy._isSpawned == false && _enemy._canSpawn == true)
        {
            _timer += Time.deltaTime;
                
            
        }
        if (_enemy._isSpawned == true)
        {
            _timer = 0;
        }
    }
}
