using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteractable : Interactables
{
    [SerializeField]
    private KeyCount _keySO;
    [SerializeField]
    private GameObject _closeDoor;
    [SerializeField]
    private GameObject _openDoor;
    [SerializeField]
    private string _keyName;
    [SerializeField]
    private Collider2D _collider;

    private void Start()
    {
        _collider = GetComponent<Collider2D>();
    }

    public override void OnInteracted()
    {
        var keyList = _keySO.Value;
        if (keyList != null)
        {
            if (keyList.Contains(_keyName))
            {
                _closeDoor.SetActive(false);
                _openDoor.SetActive(true);
                keyList.Remove(_keyName);
                _keySO.Value = keyList;
                _collider.enabled = false;
            }
        }
    }
}
