using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderGridCreator : MonoBehaviour
{    
    [SerializeField]
    private int _columnLength;
    [SerializeField]
    private int _rowLength;
    [SerializeField]
    private float x_Space;
    [SerializeField]
    private float y_Space;
    [SerializeField]
    private GameObject _colliderObject;

    internal void InSameCollider(PlayerCollision playerCollision)
    {
        if (_mainCamera.transform.position != new Vector3(playerCollision.transform.position.x, playerCollision.transform.position.y, -10f))
        {
            _mainCamera.transform.position = new Vector3(playerCollision.transform.position.x, playerCollision.transform.position.y, -10f);
        }
    }

    Camera _mainCamera;
    private void Start()
    {
        _mainCamera = Camera.main;
        for (int i = 0; i < _columnLength*_rowLength; i++)
        {
           var collider =  Instantiate(_colliderObject, new Vector3(x_Space*(i%_columnLength),-y_Space*(i/_columnLength)), Quaternion.identity, transform);
            collider.GetComponent<PlayerCollision>().ColliderGridCreator = this;
        }
    }

    public void EnteredNewCollider(PlayerCollision col)
    {
        _mainCamera.transform.position = new Vector3(col.transform.position.x, col.transform.position.y, -10f);
    }
}
