using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class InteracbleChange : MonoBehaviour
{
    [SerializeField]
    private LetterCount _letterAsset;
    [SerializeField]
    private KeyCount _keyAsset;
    [SerializeField]
    private BatteryCount _batAsset;
    //[SerializeField]
    //private LetterCount _batteryAsset;
    [SerializeField] private Text _onLetterChange;
    [SerializeField] private Text _onKeyChange;
    [SerializeField] private Text _onBatChange;


    private void OnEnable()
    {
        _letterAsset.Observers += LetterCountChange;
        _keyAsset.Observers += KeyCountChange;
        _batAsset.Observers += BatCountChange;
    }

    private void OnDisable()
    {
        _letterAsset.Observers -= LetterCountChange;
        _keyAsset.Observers -= KeyCountChange;
        _batAsset.Observers -= BatCountChange;
    }

    private void BatCountChange(int bat)
    {
        _onBatChange.text = bat.ToString();
    }

    private void KeyCountChange(List<string> key)
    {
        _onKeyChange.text = key.Count.ToString();
    }

    private void LetterCountChange(List<string> letter)
    {
        _onLetterChange.text = letter.Count.ToString();
    }
}
