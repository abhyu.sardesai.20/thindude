using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    private GameObject _player;
    [SerializeField]
    private int _boundary = 50;
    [SerializeField]
    private int _speed = 5;

    [SerializeField]
    private int _theScreenWidth;
    [SerializeField]
    private int _theScreenHeight;
    Vector3 centerOfScreen = Vector3.zero;
    Vector3 initPos = new Vector3(0,0,-10f);

    private void Start()
    {
        _theScreenHeight = Screen.height;
        _theScreenWidth = Screen.width;
    }

    private void Update()
    {
        Vector3 centerOfScreen = Vector3.zero;

        if (_player.transform.position.x * 100 > _theScreenWidth)
        {
            //transform.position = initPos + new Vector3(_theScreenWidth, 0, 0f);
            //centerOfScreen = _theScreenWidth;
            transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(_theScreenWidth , transform.position.y, transform.position.z),
                _speed * Time.deltaTime);

        }
        if (_player.transform.position.x*100 < -(_theScreenWidth + _boundary))
        {
            transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(transform.position.x - (_theScreenWidth / 2), transform.position.y, transform.position.z),
                _speed * Time.deltaTime);
        }
        if (_player.transform.position.y*100 > _theScreenHeight - _boundary)
        {
            transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(transform.position.x, transform.position.y + (_theScreenHeight / 2), transform.position.z),
                _speed * Time.deltaTime);
        }
        if (_player.transform.position.y*100 < -(_theScreenHeight + _boundary))
        {
            transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(transform.position.x, transform.position.y - (_theScreenHeight / 2), transform.position.z),
                _speed * Time.deltaTime);
        }
        Debug.Log($"{_player.transform.position.x*100},{_player.transform.position.y*100}");

    }
}
