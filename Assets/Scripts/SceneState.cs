using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable/States/Scene")]
public class SceneState : TState<int>
{
}
