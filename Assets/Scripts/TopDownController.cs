using CodeMonkey.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownController : MonoBehaviour
{
    [SerializeField]
    private BatteryCount _batteryCount;
    [SerializeField]
    private float _flashWaitTime;
    [SerializeField]
    private GameState _gameState;
    [SerializeField]
    private PlayerState _pState;
    [SerializeField]
    private FieldOfView _fieldOfView;
    [SerializeField]
    private FieldOfView _circleOfView;
    [SerializeField]
    private TMovementSprites m_Sprites;
    [SerializeField]
    private float _moveSpeed;
    [SerializeField]
    private float _sprintSpeed;
    [SerializeField]
    private float frameRate;
    [SerializeField]
    private SpriteRenderer[] _spriteRenderer;
   
    private float _speed;
    private InputListener _inputListener;
    private Rigidbody2D _rb;
    float idleTime;
    private States _state;
    private bool _flipped;
    private Interactables _interactingObject;
    private bool _canFlash = true;
    private bool _playerDead = false;
    //Audio
    private AudioSource _audioSource;
    [SerializeField] private AudioClip _interactSFX;
    [SerializeField] private AudioClip _flashSFX;
    [SerializeField] private AudioClip _deathSFX;


    private void OnEnable()
    {
        _gameState.Observers += ChangeState;
        _pState.Observers += CheckPlayerState;
    }

    private void CheckPlayerState(PState obj)
    {
        if(obj == PState.DEAD)
        {
            _playerDead = true;
            _audioSource.PlayOneShot(_deathSFX);
        }
    }

    private void OnDisable()
    {
        _gameState.Observers -= ChangeState;
        _pState.Observers -= CheckPlayerState;
    }

    private void ChangeState(States s)
    {
        _state = s;
    }

    private void Start()
    {
        _inputListener = GetComponent<InputListener>();
        _rb = GetComponent<Rigidbody2D>();
        _audioSource = GetComponentInChildren<AudioSource>();
        //_spriteRenderer = GetComponentInChildren<SpriteRenderer>();       
    }

    private void Update()
    {
        if (_state == States.PAUSE) return;
        if (_playerDead) return;
        if (_inputListener.interact)
        {
            _speed = _sprintSpeed;
        }
        else
        {
            _speed = _moveSpeed;
        }
        CheckInteraction();
        EnableFlash();
    }

    private void EnableFlash()
    {
        var batCount = _batteryCount.Value;
        if (_inputListener.shoot && _canFlash && batCount>0)
        {
            _canFlash = false;
            _audioSource.PlayOneShot(_flashSFX);
            _circleOfView.SetViewDistance(10f);
            StartCoroutine(ResetFlash());
            _batteryCount.Value--;
        }
    }

    private void CheckInteraction()
    {
        if (_inputListener.interact)        {
            
            if (_interactingObject != null)
            {
                if (!_audioSource.isPlaying)
                {
                    _audioSource.PlayOneShot(_interactSFX); 
                }
                _interactingObject.OnInteracted();
            }           
        }
    }

    IEnumerator ResetFlash()
    {
        float elapsedTime = 0.0f;
        while (elapsedTime < _flashWaitTime)
        {
            var dis= Mathf.Lerp(10, 1, (elapsedTime / _flashWaitTime));
            _circleOfView.SetViewDistance(dis);
            elapsedTime += Time.deltaTime;

            // Yield here
            yield return null;
        }       
        _canFlash = true;
        yield return null;
    }

    void FixedUpdate()
    {
        if (_state == States.PAUSE) return;
        if (_playerDead) return;
        _fieldOfView.SetOrigin(transform.position);
        _circleOfView.SetOrigin(transform.position);
        if (_inputListener.move != Vector2.zero)
        {
            _fieldOfView.SetAimDirection(new Vector3(_inputListener.move.x, _inputListener.move.y, 0));
            if (!_audioSource.isPlaying)
            {
                _audioSource.Play();
            }
        }
        //transform.Translate(_rb.position + _inputListener.move * _moveSpeed * Time.fixedDeltaTime);
        transform.position += new Vector3(_inputListener.move.x, _inputListener.move.y, 0) * _speed * Time.fixedDeltaTime;
        HandleSpriteFlip();
        List<Sprite> directionSprite = GetSpriteDirection();

        if (directionSprite != null)
        {
            float playTime = Time.time - idleTime;
            int frame = (int)(playTime * frameRate) % directionSprite.Count;
            foreach (var item in _spriteRenderer)
            {
                item.sprite = directionSprite[frame];
            }
        }
        else
        {
            idleTime = Time.time;
        }
    }

    private List<Sprite> GetSpriteDirection()
    {
        List<Sprite> selectedSprites = null;

        if (_inputListener.move.y > 0)
        {
            if (Mathf.Abs(_inputListener.move.x) > 0)
            {
                if (_flipped)
                {
                    selectedSprites = m_Sprites.upLeftSprites;
                }
                else
                {
                    selectedSprites = m_Sprites.upRightSprites;

                }
            }
            else
            {
                selectedSprites = m_Sprites.upSprites;
            }
        }
        else if (_inputListener.move.y < 0)
        {
            if (Mathf.Abs(_inputListener.move.x) > 0)
            {
                if (_flipped)
                {
                    selectedSprites = m_Sprites.downLeftSprites;
                }
                else
                {
                    selectedSprites = m_Sprites.downRightSprites;
                }
            }
            else
            {
                selectedSprites = m_Sprites.downSprites;
            }
        }
        else
        {
            if (Mathf.Abs(_inputListener.move.x) > 0)
            {
                if (_flipped)
                {
                    selectedSprites = m_Sprites.leftSprites;

                }
                else
                {
                    selectedSprites = m_Sprites.rightSprites;
                }
            }
        }
        return selectedSprites;
    }

  
    private void HandleSpriteFlip()
    {
        foreach (var item in _spriteRenderer)
        {
            if (!_flipped && _inputListener.move.x < 0)
            {
                _flipped = true;
                //item.flipX = true;
            }
            else if (_flipped && _inputListener.move.x > 0)
            {
                _flipped = false;
                //item.flipX = false;
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Interactables>() != null)
        {
            _interactingObject = collision.GetComponent<Interactables>();
        }
        if (collision.gameObject.CompareTag("EndGate"))
        {
            _pState.Value = PState.DEAD;
        }
    }


}
