using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TMovementSprites : ScriptableObject
{
    public List<Sprite> upSprites;
    public List<Sprite> downSprites;
    public List<Sprite> leftSprites;
    public List<Sprite> rightSprites;
    public List<Sprite> upLeftSprites;
    public List<Sprite> upRightSprites;
    public List<Sprite> downLeftSprites;
    public List<Sprite> downRightSprites;

}
