﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using System;

public class FieldOfView : MonoBehaviour
{
    [SerializeField]
    private PlayerState _pState;
    [SerializeField]
    private bool _shouldFlicker;
    [SerializeField]
    private bool _startFlickering;
    private float _flickerEndTime = 0f;
    [SerializeField]
    private GameObject _reaction;

    [SerializeField] private LayerMask layerMask;
    private Mesh mesh;
    [SerializeField]
    private float fov;
    [SerializeField]
    private float viewDistance;
    private Vector3 origin;
    private float startingAngle;

    private void OnEnable()
    {
        _pState.Observers += StartFlickeringLight;
    }
    private void OnDisable()
    {
        _pState.Observers -= StartFlickeringLight;
    }

    private void StartFlickeringLight(PState s)
    {
        if(s == PState.DANGER)
        {
            _startFlickering = true;
        }
        else if(s == PState.SAFE)
        {
            _startFlickering = false;
        }

        if(s == PState.DEAD)
        {
            viewDistance = 0f;
        }
    }

    private void Start() {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        //fov = 45f;
        //viewDistance = 5f;
        origin = Vector3.zero;
    }

    private void Update()
    {
        if (_shouldFlicker && _startFlickering)
        {
            if (!GetComponent<Animation>().isPlaying)
            {
                GetComponent<Animation>().Play();
            }
        }
    }

    private void LateUpdate() {
        int rayCount = 50;
        float angle = startingAngle;
        float angleIncrease = fov / rayCount;

        Vector3[] vertices = new Vector3[rayCount + 1 + 1];
        Vector2[] uv = new Vector2[vertices.Length];
        int[] triangles = new int[rayCount * 3];

        vertices[0] = origin;
        
        int vertexIndex = 1;
        int triangleIndex = 0;
        for (int i = 0; i <= rayCount; i++) {
            Vector3 vertex;
            RaycastHit2D raycastHit2D = Physics2D.Raycast(origin, UtilsClass.GetVectorFromAngle(angle), viewDistance, layerMask);
            if (raycastHit2D.collider == null) {
                // No hit
                vertex = origin + UtilsClass.GetVectorFromAngle(angle) * viewDistance;
            } else {
                // Hit object
                //Debug.Log(raycastHit2D.collider.gameObject.name);
                //_reaction.SetActive(true);
                vertex = raycastHit2D.point;
                if(raycastHit2D.collider.gameObject.CompareTag("Enemy"))
                {
                    raycastHit2D.collider.gameObject.GetComponent<EnemyAI>().Stop();
                    raycastHit2D.collider.gameObject.GetComponent<EnemyAI>().DetectedSound();                   
                }

                InteractableSound sound = raycastHit2D.collider.gameObject.GetComponent<InteractableSound>();

                if (sound != null)
                {
                    sound.PlaySound();
                }
            }
            vertex = origin + UtilsClass.GetVectorFromAngle(angle) * viewDistance;
            vertices[vertexIndex] = vertex;

            if (i > 0) {
                triangles[triangleIndex + 0] = 0;
                triangles[triangleIndex + 1] = vertexIndex - 1;
                triangles[triangleIndex + 2] = vertexIndex;

                triangleIndex += 3;
            }

            vertexIndex++;
            angle -= angleIncrease;
            //_startFlickering = false;
        }


        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;
        mesh.bounds = new Bounds(origin, Vector3.one * 1000f);
    }

    public void SetOrigin(Vector3 origin) {
        this.origin = origin;
    }

    public void SetAimDirection(Vector3 aimDirection) {
        startingAngle = UtilsClass.GetAngleFromVectorFloat(aimDirection) + fov / 2f;
    }

    public void SetFoV(float fov) {
        this.fov = fov;
    }

    public void SetViewDistance(float viewDistance) {
        this.viewDistance = viewDistance;
    }

}
